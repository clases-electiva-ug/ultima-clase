package com.jeanpier.ultima_clase;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.jeanpier.ultima_clase.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    public static final String USER_ARG = "user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.progressCircular.setVisibility(View.GONE);
        binding.buttonLogin.setOnClickListener(v -> {
            new MyTask().execute(binding.editUser.getText().toString());
        });
    }

    class MyTask extends AsyncTask<String, Void, String> {
        private String user;

        @Override
        protected void onPreExecute() {
            binding.progressCircular.setVisibility(View.VISIBLE);
            binding.buttonLogin.setEnabled(false);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Thread.sleep(5 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return strings[0];
        }

        @Override
        protected void onPostExecute(String s) {
            binding.progressCircular.setVisibility(View.INVISIBLE);
            binding.buttonLogin.setEnabled(true);
            Intent intent = new Intent(MainActivity.this, ResultActivity.class);
            intent.putExtra(USER_ARG, binding.editUser.getText().toString());
            startActivity(intent);
        }
    }
}